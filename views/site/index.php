<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>


<div class="row">
    <div class="col-12">
        <?php
            print("<br>В десятичной системе: $dec");
            printf("<br>По основанию %s: %s<br>", $base="2", $number);
        ?>
    </div>

    <div class="col-12">
        <br/>
        <br/>

        Выбирающий список владельцев топиков для заданных авторов, участвовавших в обсуждениях;<br/>

        <?php foreach ($authors as $author) {?>

            <?php echo $author->username;?><br/>

        <?php }?>

        <br/>
        <br/>

        -ТОП 10 лучших комментариев для заданного владельца;
        <br/><br/>

        <?php foreach ($tops as $tops) {?>

            <?php echo $tops->text;?><br/>

        <?php }?>

        <br/>
        <br/>

        -Выбирающий N последних лучших комментариев для заданного автора и заданного владельца.
        <br/>
        <br/>

        <?php foreach ($comments as $comment) {?>

            <?php echo $comment->text;?><br/>

        <?php }?>
    </div>
</div>
