<?php

namespace app\components;

use yii\base\Component;

/**
 * Class ReconvertComponent
 * @package app\components
 */
class ReconvertComponent extends Component
{

    /**
     * @param $bin_nr
     * @return float|int
     */
    /*
    public function convert($bin_nr)
    {
        $base=1;
        $dec_nr=0;
        $bin_nr=explode(",", preg_replace("/(.*),/", "$1", str_replace("1", "1,", str_replace("0", "0,", $bin_nr))));
        for($i=1; $i<count($bin_nr); $i++) {
            $base=$base*2;
        }
        foreach($bin_nr as $key=>$bin_nr_bit) {
            if($bin_nr_bit==1) {
                $dec_nr+=$base;
                $base=$base/2;
            }
            if($bin_nr_bit==0) $base=$base/2;
        }
        return $dec_nr;
    }*/

    /**
     * @param $entier
     * @param $base
     * @return mixed|string
     */
    public function entier_from_decimal(&$entier, $base)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";    // алфавит 36 символов
        $digit = bcmod($entier, $base);         // остаток
        $entier = bcdiv($entier, $base);        // частное
        if(empty($entier)){                     // если частное нулевое
            return $chars[$digit];              // процесс закончен
        }else{                                  // иначе - рекурсивный вызов + цифра
            return $this->entier_from_decimal($entier, $base) . $chars[$digit];
        }
    }

    /**
     * @param $frac
     * @param $base
     * @param int $scale
     * @return string
     */
    public function frac_from_decimal(&$frac, $base, $scale = 0)
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";    // алфавит 36 символов
        if($scale == 0){                        // если дробная часть не нужна
            return "";                          // возвращаем пустую строку
        }
        $len_frac = strlen($frac);              // запомнили длину
        $frac = bcmul($frac, $base);            // умножили дробную часть на основание
        $digit = 0;                             // по умолчанию цифра нуль
        if(strlen($frac) > $len_frac){              // если произведение длиннее, то
            $digit = $frac[0];                      // вырезаем первую цифру
            $frac = substr($frac, 1, $len_frac);    // и новая дробная часть - без неё
        }elseif(strlen($frac) < $len_frac){         // а если короче, то
            $frac = str_pad($frac, $len_frac, "0"); // дополняем нулями спереди
        }

        return $chars[$digit] . $this->frac_from_decimal($frac, $base, $scale-1);
    }

    /**
     * @param $dec
     * @param $base
     * @param int $scale
     * @return mixed|string
     */
    public function big_from_decimal($dec, $base, $scale = 0)
    {
        if(substr($dec,0,1) == "+"){    // учёт знака "+"
            $dec = substr($dec,1,strlen($dec)-1);
            return "+".big_from_decimal($dec, $base, $scale);
        }
        if(substr($dec,0,1) == "-"){    // учёт знака "-"
            $dec = substr($dec,1,strlen($dec)-1);
            return "-".big_from_decimal($dec, $base, $scale);
        }
        $frac = strstr($dec,".");       // дробная часть с точкой
        if(($frac==false) || (($len_frac = strlen($frac))<2)){
            return $this->entier_from_decimal($dec, $base);
        }
        $frac = substr($frac, 1 , $len_frac -1);                // удаление точки
        $entier = substr($dec, 0, strlen($dec)- $len_frac);     // целая часть
        return $this->entier_from_decimal($entier, $base). "."         // конкатенация
            .$this->frac_from_decimal($frac, $base, $scale);           // результатов
    }


}
