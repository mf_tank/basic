<?php

use yii\db\Migration;

/**
 * Class m210106_143044_tbl_three
 */
class m210106_143044_tbl_three extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('author', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ]);

        $this->createIndex(
            'author_unique',
            'author',
            'email',
            true
        );

        $this->createTable('article', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'date_published' => $this->dateTime(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ]);

        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'article_id' => $this->integer()->notNull(),
            'text' => $this->text()->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ]);

        $this->addForeignKey(
            'comment_fk',
            'comment',
            'parent_id',
            'comment',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'comment_article_fk',
            'comment',
            'article_id',
            'article',
            'id',
            'cascade',
            'cascade'
        );

        $this->createTable('author_article', [
            'id' => $this->primaryKey(),
            'author_id' => $this->integer()->notNull(),
            'article_id' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'author_article_author_fk',
            'author_article',
            'author_id',
            'author',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'author_article_article_fk',
            'author_article',
            'article_id',
            'article',
            'id',
            'cascade',
            'cascade'
        );

        $this->createIndex(
            'author_article2',
            'author_article',
            'author_id, article_id',
            true
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('author_article');
        $this->dropTable('author');
        $this->dropTable('comment');
        $this->dropTable('article');
    }


}
