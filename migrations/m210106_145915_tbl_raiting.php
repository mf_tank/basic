<?php

use yii\db\Migration;

/**
 * Class m210106_145915_tbl_raiting
 */
class m210106_145915_tbl_raiting extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('rating', [
            'id' => $this->primaryKey(),
            'comment_id' => $this->integer()->notNull(),
            'number' => $this->integer(1)->notNull(),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull()
        ]);


        $this->addForeignKey(
            'rating_comment_fk',
            'rating',
            'comment_id',
            'comment',
            'id',
            'cascade',
            'cascade'
        );

        $this->addColumn('comment', 'author_id', $this->integer()->notNull());
        $this->addForeignKey(
            'comment_author_fk',
            'comment',
            'author_id',
            'author',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('comment', 'author_id');
        $this->dropTable('rating');
    }

}
