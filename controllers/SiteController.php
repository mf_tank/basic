<?php

namespace app\controllers;

use app\components\ReconvertComponent;
use app\models\Author;
use app\models\AuthorArticle;
use app\models\Comment;
use app\models\Rating;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $convert = new ReconvertComponent();

        $dec = "5";
        $number = $convert->big_from_decimal($dec, 2, 20);

        $authors = Author::find()
            ->innerJoinWith(['authorArticles', 'comments'], false)
            ->all();

        $tops = Comment::find()->innerJoinWith(['ratings'], false)
            ->limit(10)
            ->andWhere([
                'rating.number' => 5,
                'author_id' => 1
            ])
            ->all();

        $comments = Comment::find()->innerJoinWith(['ratings', 'article', 'article.authorArticles'])
            ->andWhere([
                'rating.number' => 5,
                'comment.author_id' => 1,
                'author_article.author_id' => 1
            ])
            ->orderBy('comment.created_at desc')
            ->all();

        return $this->render('index', [
            'dec' => $dec,
            'number' => $number,
            'authors' => $authors,
            'tops' => $tops,
            'comments' => $comments
        ]);

    }


}
